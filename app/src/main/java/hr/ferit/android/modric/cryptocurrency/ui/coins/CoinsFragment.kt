package hr.ferit.android.modric.cryptocurrency.ui.coins

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import hr.ferit.android.modric.cryptocurrency.databinding.FragmentCoinsBinding


class CoinsFragment : Fragment() {

    private val coinsViewModel: CoinsViewModel by lazy {
        ViewModelProvider(this).get(CoinsViewModel::class.java)
    }

    private lateinit var binding: FragmentCoinsBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCoinsBinding.inflate(inflater)

        binding.lifecycleOwner = this

        binding.coinsViewModel = coinsViewModel

        binding.rvCoins.adapter = CoinAdapter(CoinAdapter.OnCoinClickListener { coinMarket ->
            coinsViewModel.displayCoinDetails(coinMarket)
        })

        coinsViewModel.navigateToSelectedMarket.observe(viewLifecycleOwner, Observer {
            if (null != it) {
                this.findNavController().navigate(
                    CoinsFragmentDirections.actionShowDetail(
                        it,
                        it.name
                    )
                )
                coinsViewModel.displayCoinDetailsComplete()
            }
        })

        coinsViewModel.status.observe(viewLifecycleOwner, Observer {
            if (it == CoinApiStatus.LOADING) {
                showProgressDialog()
                binding.rvCoins.visibility = View.GONE
            } else if (it == CoinApiStatus.DONE || it == CoinApiStatus.ERROR) {
                hideProgressDialog()
                binding.rvCoins.visibility = View.VISIBLE
            }
        })

        binding.srlCoins.setOnRefreshListener {
            binding.srlCoins.isRefreshing = false
            coinsViewModel.getCoinsMarkets()
        }

        return binding.root
    }

    private fun showProgressDialog() {
        binding.progressBar.visibility = View.VISIBLE
    }

    private fun hideProgressDialog() {
        binding.progressBar.visibility = View.GONE
    }

}