package hr.ferit.android.modric.cryptocurrency.ui.detail

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import hr.ferit.android.modric.cryptocurrency.databinding.FragmentDetailBinding
import hr.ferit.android.modric.cryptocurrency.ui.coins.CoinApiStatus


class DetailFragment : Fragment() {
    private lateinit var detailViewModel: DetailViewModel
    private lateinit var binding: FragmentDetailBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        val application = requireNotNull(activity).application
        binding = FragmentDetailBinding.inflate(inflater)
        binding.lifecycleOwner = this

        val coinMarket = DetailFragmentArgs.fromBundle(arguments!!).selectedCoin
        val viewModelFactory = DetailViewModelFactory(coinMarket, application)

        detailViewModel = ViewModelProvider(this, viewModelFactory).get(DetailViewModel::class.java)
        binding.detailViewModel = detailViewModel

        detailViewModel.status.observe(viewLifecycleOwner, Observer {
            if (it == LoadingChartDataStatus.LOADING) {
                showProgressDialog()
            } else if (it == LoadingChartDataStatus.DONE || it == LoadingChartDataStatus.ERROR) {
                hideProgressDialog()
            }
        })

        return binding.root
    }

    private fun showProgressDialog() {
        binding.chartProgressBar.visibility = View.VISIBLE
    }

    private fun hideProgressDialog() {
        binding.chartProgressBar.visibility = View.GONE
    }
}