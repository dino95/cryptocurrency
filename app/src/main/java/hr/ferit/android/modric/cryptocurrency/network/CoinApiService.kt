package hr.ferit.android.modric.cryptocurrency.network

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.coroutines.Deferred
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


private const val BASE_URL = "https://api.coingecko.com/api/v3/coins/"

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private val retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .addCallAdapterFactory(CoroutineCallAdapterFactory())
    .baseUrl(BASE_URL)
    .build()

interface CoinApiService {
    @GET("markets?vs_currency=eur&order=market_cap_desc&per_page=50&page=1&sparkline=false")
    fun getCoinsMarkets():
            Deferred<List<CoinMarket>>

    @GET("{id}/market_chart?vs_currency=eur")
    fun getMarketCharts(@Path("id") id: String, @Query("days") days: Int):
            Deferred<MarketChart>
}

object CoinApi {
    val retrofitService : CoinApiService by lazy {
        retrofit.create(CoinApiService::class.java)
    }
}