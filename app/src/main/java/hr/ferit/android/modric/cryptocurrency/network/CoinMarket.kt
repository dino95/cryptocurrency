package hr.ferit.android.modric.cryptocurrency.network

import android.icu.text.NumberFormat
import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize
import java.util.*

enum class CoinPriceStatus {
    UP,
    DOWN
}

@Parcelize
data class CoinMarket(
    val id: String,

    val name: String,

    @Json(name = "image")
    val imageUrlSrc: String,

    @Json(name = "current_price")
    val currentPrice: Double,

    @Json(name = "market_cap")
    val marketCap: Long,

    @Json(name = "market_cap_rank")
    val marketCapRank: Int,

    @Json(name = "high_24h")
    val high24h: Double,

    @Json(name = "low_24h")
    val low24h: Double,

    @Json(name = "price_change_24h")
    val priceChange24h: Double,

    @Json(name = "price_change_percentage_24h")
    val priceChangePercentage24h: Double,

    @Json(name = "market_cap_change_24h")
    val marketCapChange24h: Double,

    @Json(name = "market_cap_change_percentage_24h")
    val marketCapChangePercentage24h: Double
) : Parcelable {
    val currentPriceString = "€ " + NumberFormat.getNumberInstance(Locale.GERMANY).format(currentPrice)!!
    val marketCapString = "€ " + NumberFormat.getNumberInstance(Locale.GERMANY).format(marketCap)!!
    val marketCapRankString = "$marketCapRank."
    val priceChange24hString = "€ " + NumberFormat.getNumberInstance(Locale.GERMANY).format(priceChange24h)!!
    val priceChangePercentage24hString = String.format("%.2f%%", priceChangePercentage24h)
    val marketCapPercentage24hString = String.format("%.2f%%", marketCapChangePercentage24h)
    val typeOfPriceChange24h = when {
        priceChangePercentage24h > 0.0 -> CoinPriceStatus.UP
        priceChangePercentage24h < 0.0 -> CoinPriceStatus.DOWN
        else -> CoinPriceStatus.UP
    }
    val low24hString = "€ " + NumberFormat.getNumberInstance(Locale.GERMANY).format(low24h)!!
    val high24hString = "€ " + NumberFormat.getNumberInstance(Locale.GERMANY).format(high24h)!!

}