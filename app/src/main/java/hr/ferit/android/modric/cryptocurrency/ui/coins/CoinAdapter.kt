package hr.ferit.android.modric.cryptocurrency.ui.coins

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import hr.ferit.android.modric.cryptocurrency.databinding.CoinItemBinding
import hr.ferit.android.modric.cryptocurrency.network.CoinMarket


class CoinAdapter(private val onClickListener: OnCoinClickListener) : androidx.recyclerview.widget.ListAdapter<CoinMarket, CoinAdapter.CoinMarketViewHolder>(
    DiffCallback
) {

    class CoinMarketViewHolder(private var binding: CoinItemBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(coinMarket: CoinMarket) {
            binding.property = coinMarket
            binding.executePendingBindings()
        }
    }

    companion object DiffCallback : DiffUtil.ItemCallback<CoinMarket>() {
        override fun areItemsTheSame(oldItem: CoinMarket, newItem: CoinMarket): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: CoinMarket, newItem: CoinMarket): Boolean {
            return oldItem.id == newItem.id
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CoinMarketViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = CoinItemBinding.inflate(layoutInflater, parent, false)
        return CoinMarketViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CoinMarketViewHolder, position: Int) {
        val coinMarket = getItem(position)
        holder.itemView.setOnClickListener {
            onClickListener.onClick(coinMarket)
        }
        holder.bind(coinMarket)
    }

    class OnCoinClickListener(val clickListener: (coinMarket: CoinMarket) -> Unit) {
        fun onClick(coinMarket: CoinMarket) = clickListener(coinMarket)
    }


}