package hr.ferit.android.modric.cryptocurrency

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import androidx.core.net.toUri
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.request.RequestOptions
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import hr.ferit.android.modric.cryptocurrency.R
import hr.ferit.android.modric.cryptocurrency.network.CoinMarket
import hr.ferit.android.modric.cryptocurrency.network.CoinPriceStatus
import hr.ferit.android.modric.cryptocurrency.ui.coins.CoinAdapter
import hr.ferit.android.modric.cryptocurrency.ui.coins.CoinApiStatus
import hr.ferit.android.modric.cryptocurrency.ui.detail.DetailViewModel
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


@BindingAdapter("listData")
fun bindRecyclerView(recyclerView: RecyclerView, data: List<CoinMarket>?) {
    val adapter = recyclerView.adapter as CoinAdapter
    adapter.submitList(data)
}

@BindingAdapter("imageUrl")
fun bindImage(imgView: ImageView, imgUrl: String?) {
    imgUrl?.let {
        val imgUri = imgUrl.toUri().buildUpon().scheme("https").build()
        Glide.with(imgView.context)
            .load(imgUri)
            .apply(
                RequestOptions()
                    .placeholder(R.drawable.loading_animation)
                    .error(R.drawable.ic_broken_image)
            )
            .into(imgView)
    }
}

@BindingAdapter("coinApiStatus")
fun bindStatus(statusImageView: ImageView, status: CoinApiStatus?) {
    when(status) {
        CoinApiStatus.LOADING -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.loading_animation)
        }
        CoinApiStatus.ERROR -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.ic_connection_error)
        }
        CoinApiStatus.DONE -> {
            statusImageView.visibility = View.GONE
        }

    }
}

@BindingAdapter("coinPriceStatus")
fun bindPriceStatus(priceStatusImageView: ImageView, status: CoinPriceStatus) {
    when(status) {
        CoinPriceStatus.UP -> {
            priceStatusImageView.setImageResource(R.drawable.ic_current_price_up)
        }
        CoinPriceStatus.DOWN -> {
            priceStatusImageView.setImageResource(R.drawable.ic_current_price_down)
        }
    }
}

private fun getTimeAxisFormat(timestamp: String, timeRange: Int): String? {
    return try {
        val dateFormat= when (timeRange) {
            1 -> SimpleDateFormat("hh:mm")
            7, 30 -> SimpleDateFormat("dd MMM")
            365-> SimpleDateFormat("MMM yyyy")
            else -> SimpleDateFormat("hh:mm")
        }
        val dateTime = Date(timestamp.toLong())
        dateFormat.format(dateTime).toString()
    } catch (e: Exception) {
        e.toString()
    }
}


@BindingAdapter("setLineData", "setTimeRange", "axisColor")
fun setLineData(barChartView: BarChart, data: List<List<String>>?, timeRange: Int, axisColor: Int) {
    val currentPrice = mutableListOf<BarEntry>()
    val timeAxisLabel: ArrayList<String> = ArrayList()
    var count = 0

    data?.forEach { item ->
        currentPrice.add(BarEntry(count.toFloat(), item[1].toFloat()))
        getTimeAxisFormat(item[0], timeRange)?.let { timeAxisLabel.add(it) }
        count++
    }

    val currentPriceAxis = BarDataSet(currentPrice, "Current Price")
    var marketData = java.util.ArrayList<IBarDataSet>()
    currentPriceAxis.color = axisColor
    marketData.add(currentPriceAxis)

    val barData = BarData(marketData)

    barChartView.data = barData
    barChartView.legend.isEnabled = false
    barChartView.invalidate()
    barChartView.axisRight.isEnabled = false
    barChartView.axisLeft.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART)
    barChartView.axisRight.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART)


    val xAxisChart = barChartView.xAxis
    xAxisChart.position = XAxis.XAxisPosition.BOTTOM
    xAxisChart.setDrawGridLines(false)
    xAxisChart.labelCount = 6
    xAxisChart.granularity = 1f
    xAxisChart.isGranularityEnabled = true

    xAxisChart.valueFormatter = (DetailViewModel.TimeValueFormatter(timeAxisLabel))
}
