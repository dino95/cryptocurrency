package hr.ferit.android.modric.cryptocurrency.ui.coins

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import hr.ferit.android.modric.cryptocurrency.network.CoinApi
import hr.ferit.android.modric.cryptocurrency.network.CoinMarket
import java.lang.Exception

enum class CoinApiStatus {
    LOADING,
    ERROR,
    DONE
}

class CoinsViewModel : ViewModel() {
    private val _status = MutableLiveData<CoinApiStatus>()
    val status: LiveData<CoinApiStatus>
        get() = _status

    private val _properties = MutableLiveData<List<CoinMarket>>()
    val properties: LiveData<List<CoinMarket>>
        get() = _properties

    private var viewModelJob = Job()

    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    private val _navigateToSelectedCoin = MutableLiveData<CoinMarket>()
    val navigateToSelectedMarket: LiveData<CoinMarket>
        get() = _navigateToSelectedCoin

    init {
        getCoinsMarkets()
    }

    fun getCoinsMarkets() {
        coroutineScope.launch {
            var getPropertiesDeferred = CoinApi.retrofitService.getCoinsMarkets()
            try {
                _status.value = CoinApiStatus.LOADING
                var coinsMarkets = getPropertiesDeferred.await()
                _status.value = CoinApiStatus.DONE
                _properties.value = coinsMarkets
            } catch (error: Exception) {
                _status.value = CoinApiStatus.ERROR
                _properties.value = ArrayList()
            }
        }
    }

    fun displayCoinDetails(coinMarket: CoinMarket) {
        _navigateToSelectedCoin.value = coinMarket
    }

    fun displayCoinDetailsComplete() {
        _navigateToSelectedCoin.value = null
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
}